<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $request){
        // dd($request->all());
        
        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];
        return view('wellcome', compact('namadepan', 'namabelakang'));
    }
}