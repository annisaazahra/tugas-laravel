@extends('layout.master')
@section('judul')
Halaman Edit Cast 
@endsection

@section('content')
<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put');
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div>
        <label>Umur</label>
        <input type="text" name="umur" value="{{$cast->umur}}" class="form-control"><br>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label>Bio</label>
        <textarea name="bio" cols="20" rows="10" class="form-control">{{$cast->bio}}</textarea> <br>
    </div>
    
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection