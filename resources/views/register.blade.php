@extends('layout.master')
@section('judul')
Buat Account Baru
@endsection

@section('content')
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name :</label><br>
        <input type="text" name="namadepan"><br>
        <label>Last name :</label><br>
        <input type="text" name="namabelakang"><br>
        <label>Gender</label><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <label>Nationality</label><br>
        <select name="kebangsaan" id="">
            <option value="1">Indonesia</option>
            <option value="2">Asing</option>
        </select><br><br>
        <label>Language Spoken</label><br>
        <input type="checkbox" name="bahasa"> Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa"> English <br>
        <input type="checkbox" name="bahasa"> Other <br>
        <label>Bio</label><br>
        <textarea name="bio" cols="30" rows="5"></textarea><br>
        <input type="submit" value="Sign Up">


    </form>
@endsection