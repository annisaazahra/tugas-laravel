<?php
use Symfony\Component\HttpKernel\Fragment\RoutableFragmentRenderer;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::get('/', function () {
//     return view('welcome');
// });



Route::get('/', 'IndexController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

// Route::get('/welcome', function($nama){
//     return view('welcome');
// });

// Route::get('/master', function(){
//     return view('layout.master');
// });

// Route::get('/data-tables', 'IndexController@datatables');

Route::get('/data-tables', function(){
    return view('table.data-tables');
});

//CRUD Cast
//create
Route::get('/cast/create', 'CastController@create'); //mengarah ke form tambah data
Route::post('/cast', 'CastController@store'); //menyimpan data form ke database table cast

//read
Route::get('/cast', 'CastController@index'); //ambil data di db kemudian  di tampilkan di blade
Route::get('/cast/{cast_id}', 'CastController@show'); //Route detail cast

//update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route untuk mengarah ke form edit

Route::put('/cast/{cast_id}', 'CastController@update'); //Route untuk melakukan update

//delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Route untuk menghapus data